---
title: Fathom
icon: icon.svg
replaces:
    - google-analytics
---

**Fathom** is [open source][floss] analytics software that avoids tracking or storing personal data. The self-hosted version is free, and there are paid accounts that are hosted by the makers of Fathom.

{{< infobox >}}
- **Website:** 
    - [usefathom.com](https://usefathom.com)
- **Source Code:** 
    - [github.com](https://github.com/usefathom/fathom)
{{< /infobox >}}

[floss]: https://web.archive.org/web/20180904102804/https://switching.social/what-is-open-source-software/
