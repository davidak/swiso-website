---
title: Libre.fm
icon: icon.svg
replaces:
    - amazon-books
---

**Libre.fm** offer [DRM-free][drm] audiobooks while also supporting local bookstores. Their membership option is [only available in the US and Canada][availability] though.

{{< infobox >}}
- **Website:** 
    - [Libre.fm](https://libro.fm/)
{{< /infobox >}}

[drm]: {{< relref "/articles/digital-rights-management" >}}
[availability]: https://librofm.freshdesk.com/support/solutions/articles/48000695413-where-is-libro-fm-available-